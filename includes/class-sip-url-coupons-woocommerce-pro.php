<?php 
class Sip_Url_Coupons_Woocommerce_Pro {
	public function __construct() {

		$this->define_sip_keys_pro();
        add_action( 'sip_wc_url_coupons_before_coupon_applied', array( $this, 'maybe_add_products_to_cart' ), 100 );
		add_action( 'woocommerce_coupon_options',               array( $this, 'add_per_coupon_options' ), 10, 2 );
		add_action( 'woocommerce_coupon_options_save',          array( $this, 'save_per_coupon_options' ), 10, 2 );
		add_filter( 'sip_wc_url_coupons_redirect_url',          array( $this, 'get_redirect_url' ), 10, 3 );
		if ( '1' === get_option( SIP_WC_URL_COUPONS_DELAY_NOTICE ) ) {
			add_action( 'sip_wc_url_coupons_after_coupon_applied', array( $this, 'delay_notice' ), 10, 3 );
			add_action( 'wp_head', array( $this, 'display_delayed_notice' ) );
		}
		add_action( 'sip_wc_url_coupons_after_coupon_applied',  array( $this, 'add_notice' ), 9, 3 );
		add_action( 'woocommerce_coupon_message',               array( $this, 'remove_default_notice' ), 9, 3 );
		add_action( 'woocommerce_coupon_message',               array( $this, 'append_notice' ), 10, 3 );
		add_shortcode( 'sip_wc_url_coupons_print_notices',      array( $this, 'print_notices' ) );
       
	}
    /**
	 * maybe_add_products_to_cart.
	 *
	 * @version 1.4.0
	 * @since   1.0.0
	 * @todo    [maybe] check if coupon is valid
	 */
    public function define_sip_keys_pro(){
		define( 'SIP_WC_URL_COUPONS_FIXED_PRODUCT_DISCOUNT_ADD_PRODUCTS', 'sip_wc_url_coupons_fixed_product_discount_add_products' );
		define( 'SIP_WC_URL_COUPONS_FIXED_PRODUCT_DISCOUNT_ADD_PRODUCTS_EMPTYCART', 'sip_wc_url_coupons_fixed_product_discount_add_products_emptycart' ); 
		
		define( 'SIP_WC_URL_COUPONS_REDIRECT', 'sip_wc_url_coupons_redirect' );
		define( 'SIP_WC_URL_COUPONS_REDIRECT_CUSTOM_URL', 'sip_wc_url_coupons_redirect_custom_url' ); 
		define( 'SIP_WC_URL_COUPONS_REDIRECT_PER_COUPON', 'sip_wc_url_coupons_redirect_per_coupon' );
		
		define( 'SIP_WC_URL_COUPONS_DELAY_NOTICE', 'sip_wc_url_coupons_delay_notice' ); 
		define( 'SIP_WC_URL_COUPONS_NOTICE', 'sip_wc_url_coupons_notice' ); 
		define( 'SIP_WC_URL_COUPONS_NOTICE_PER_COUPON', 'sip_wc_url_coupons_notice_per_coupon' ); 
		define( 'SIP_WC_URL_COUPONS_NOTICE_PER_COUPON_OVERRIDE', 'sip_wc_url_coupons_notice_per_coupon_override' ); 
		define( 'SIP_WC_URL_COUPONS_NOTICE_REMOVE_DEFAULT', 'sip_wc_url_coupons_notice_remove_default' ); 
		define( 'SIP_WC_URL_COUPONS_NOTICE_METHOD', 'sip_wc_url_coupons_notice_method' ); 
		define( 'SIP_WC_URL_COUPONS_NOTICE_TYPE', 'sip_wc_url_coupons_notice_type' ); 
		define( 'SIP_WC_URL_COUPONS_NOTICE_GLUE', 'sip_wc_url_coupons_notice_glue' ); 

	}
    	/**
	 * is_product_in_cart.
	 *
	 * @version 1.0.0
	 * @since   1.0.0
	 * @todo    [maybe] https://woocommerce.github.io/code-reference/classes/WC-Cart.html#method_find_product_in_cart
	 */
	public function is_product_in_cart( $product_id ) {
		if ( 0 != $product_id ) {            
			if ( isset( WC()->cart->cart_contents ) && is_array( WC()->cart->cart_contents ) ) {                            
				foreach ( WC()->cart->cart_contents as $cart_item_key => $cart_item_data ) {                   
					if (
						( isset( $cart_item_data['product_id'] )   && $product_id == $cart_item_data['product_id'] ) ||
						( isset( $cart_item_data['variation_id'] ) && $product_id == $cart_item_data['variation_id'] )
					) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public function maybe_add_products_to_cart( $coupon_code ) {       
		if ( 'no' === get_option( SIP_WC_URL_COUPONS_FIXED_PRODUCT_DISCOUNT_ADD_PRODUCTS, 'no' ) ) {
			return false;
		}
		// Coupons are globally disabled
		if ( ! wc_coupons_enabled() ) {
			return false;
		}
		// Maybe clear the cart    
        
		if ( get_option( SIP_WC_URL_COUPONS_FIXED_PRODUCT_DISCOUNT_ADD_PRODUCTS_EMPTYCART ) ) {			
            WC()->cart->empty_cart();
		}
		// Sanitize coupon code
		$coupon_code = wc_format_coupon_code( $coupon_code );
		// Get the coupon
		$the_coupon = new WC_Coupon( $coupon_code );
		if ( 'fixed_product' === $the_coupon->get_discount_type() ) {
            
			$product_ids = $the_coupon->get_product_ids();         
			if ( ! empty( $product_ids ) ) {
				foreach ( $product_ids as $product_id ) {                    
					if ( ! $this->is_product_in_cart( $product_id ) ) {
						WC()->cart->add_to_cart( $product_id );
					}
				}
			}
		}
		return true;
	}
	/**
	 * get_redirect_url.
	 *
	 * @version 1.4.0
	 * @since   1.0.0
	 */
	public function get_redirect_url( $url, $coupon_code, $key ) {
		if (
			'1' === get_option( SIP_WC_URL_COUPONS_REDIRECT_PER_COUPON ) &&
			0 != ( $coupon_id = wc_get_coupon_id_by_code( $coupon_code ) ) &&
			'' != ( $per_coupon_url = get_post_meta( $coupon_id, '_sip_wc_url_coupons_redirect_url', true ) )
		) {		
			return $per_coupon_url;
		}
		switch ( get_option( SIP_WC_URL_COUPONS_REDIRECT, 'no' ) ) {
			case 'cart':
				return wc_get_cart_url();
			case 'checkout':
				return wc_get_checkout_url();
			case 'custom':
				return get_option( SIP_WC_URL_COUPONS_REDIRECT_CUSTOM_URL );
		}
		return $url;
	}
	/**
	 * add_per_coupon_options.
	 *
	 * @version 1.4.0
	 * @since   1.4.0
	 * @todo    [maybe] `_sip_wc_url_coupons_redirect_url`: `'type' => 'url'`?
	 * @todo    [maybe] `_sip_wc_url_coupons_notice`: `woocommerce_wp_text_input()` instead of `woocommerce_wp_textarea_input()`?
	 */
	function add_per_coupon_options( $coupon_id, $coupon ) {
		if (  '1'=== get_option( SIP_WC_URL_COUPONS_REDIRECT_PER_COUPON ) ) {
			woocommerce_wp_text_input(
				array(
					'id'          => '_sip_wc_url_coupons_redirect_url',
					'label'       => __( 'URL Coupons', 'url-coupons-for-woocommerce-by-shopitpress' ) . ': ' . __( 'Redirect URL', 'url-coupons-for-woocommerce-by-shopitpress' ),
					'placeholder' => '',
					'description' => sprintf( __( 'For the "%s" plugin.', 'url-coupons-for-woocommerce-by-shopitpress' ), __( 'URL Coupons for WooCommerce', 'url-coupons-for-woocommerce-by-shopitpress' ) ),
					'data_type'   => 'url',
					'desc_tip'    => true,
					'value'       => get_post_meta( $coupon_id, '_sip_wc_url_coupons_redirect_url', true ),
				)
			);
		}
		if ( '1' === get_option( SIP_WC_URL_COUPONS_NOTICE_PER_COUPON ) ) {
			woocommerce_wp_textarea_input(
				array(
					'id'          => '_sip_wc_url_coupons_notice',
					'label'       => __( 'URL Coupons', 'url-coupons-for-woocommerce-by-shopitpress' ) . ': ' . __( 'Notice', 'url-coupons-for-woocommerce-by-shopitpress' ),
					'description' => sprintf( __( 'For the "%s" plugin.', 'url-coupons-for-woocommerce-by-shopitpress' ), __( 'URL Coupons for WooCommerce', 'url-coupons-for-woocommerce-by-shopitpress' ) ),
					'desc_tip'    => true,
					'value'       => get_post_meta( $coupon_id, '_sip_wc_url_coupons_notice', true ),
				)
			);
		}
	}

	/**
	 * save_per_coupon_options.
	 *
	 * @version 1.4.0
	 * @since   1.4.0
	 * @see     https://developer.wordpress.org/reference/functions/wp_kses_post/
	 * @see     https://developer.wordpress.org/reference/functions/wp_unslash/
	 * @see     https://github.com/woocommerce/woocommerce/blob/4.9.0/includes/admin/class-wc-admin-settings.php#L771
	 * @see     https://github.com/woocommerce/woocommerce/blob/4.9.0/includes/admin/class-wc-admin-meta-boxes.php#L198
	 * @todo    [next] [!] `_sip_wc_url_coupons_notice`: raw || `sanitize_textarea_field()` || `wc_clean()` || ... ?
	 * @todo    [maybe] `_sip_wc_url_coupons_redirect_url`: recheck `wc_clean()`?
	 * @todo    [maybe] nonce?
	 */
	function save_per_coupon_options( $coupon_id, $coupon ) {
		if ( '1'=== get_option( SIP_WC_URL_COUPONS_REDIRECT_PER_COUPON ) ) {
			if ( isset( $_POST['_sip_wc_url_coupons_redirect_url'] ) ) {
				update_post_meta( $coupon_id, '_sip_wc_url_coupons_redirect_url', wc_clean( $_POST['_sip_wc_url_coupons_redirect_url'] ) );
			}
		}
		if ( '1' === get_option( SIP_WC_URL_COUPONS_NOTICE_PER_COUPON ) ) {
			if ( isset( $_POST['_sip_wc_url_coupons_notice'] ) ) {
				update_post_meta( $coupon_id, '_sip_wc_url_coupons_notice', wp_kses_post( trim( wp_unslash( $_POST['_sip_wc_url_coupons_notice'] ) ) ) );
			}
		}
	}
	/**
	 * delay_notice.
	 *
	 * @version 1.4.0
	 * @since   1.3.0
	 * @todo    [maybe] still delay notice on `! $result`?
	 */
	function delay_notice( $coupon_code, $key, $result ) {
		if ( ! $result ) {
			return;
		}
		if ( WC()->cart->is_empty() ) {
			$all_notices = WC()->session->get( 'wc_notices', array() );
			wc_clear_notices();
			WC()->session->set( 'sip_wc_url_coupons_notices', $all_notices );
		}
	}
		/**
	 * display_delayed_notice.
	 *
	 * @version 1.3.0
	 * @since   1.2.5
	 */
	function display_delayed_notice() {
		if ( function_exists( 'WC' ) && ! WC()->cart->is_empty() && ( $notices = WC()->session->get( 'sip_wc_url_coupons_notices', array() ) ) && ! empty( $notices ) ) {
			WC()->session->set( 'sip_wc_url_coupons_notices', null );
			WC()->session->set( 'wc_notices', $notices );
		}
	}
	/**
	 * add_notice.
	 *
	 * @version 1.4.0
	 * @since   1.4.0
	 */
	
	 function add_notice( $coupon_code, $key, $result ) {
		if ( ! $result ) {
			return;
		}
		if ( 'add' != get_option( SIP_WC_URL_COUPONS_NOTICE_METHOD ) ) {
			return;
		}
		$notices = $this->get_notices( $coupon_code );
		$type    = get_option( SIP_WC_URL_COUPONS_NOTICE_TYPE );
		foreach ( $notices as $notice ) {
			wc_add_notice( $notice, $type );
		}	
	}
	/**
	 * get_notices.
	 *
	 * @version 1.4.0
	 * @since   1.4.0
	 * @todo    [maybe] show notice even for non-URL coupon use?
	 * @todo    [maybe] different notices on `! $result` (or ! `WC_Coupon::WC_COUPON_SUCCESS`)?
	 */
	function get_notices( $coupon, $current_notice = false ) {
		$notices = array();
		if ( $current_notice ) {
			$notices[] = $current_notice;
		}
		if (
			'1' === get_option( SIP_WC_URL_COUPONS_NOTICE_PER_COUPON ) &&
			0 != ( $coupon_id = ( is_a( $coupon, 'WC_Coupon' ) ? $coupon->get_id() : wc_get_coupon_id_by_code( $coupon ) ) ) &&
			'' != ( $per_coupon_notice = get_post_meta( $coupon_id, '_sip_wc_url_coupons_notice', true ) )
		) {
			$notices[] = $per_coupon_notice;
			if ( '1' != get_option( SIP_WC_URL_COUPONS_NOTICE_PER_COUPON_OVERRIDE ) ) {
				return apply_filters( 'sip_wc_url_coupons_get_notices', array_unique( array_filter( $notices ) ) );
			}
		}
		if ( '' != ( $notice = get_option( SIP_WC_URL_COUPONS_NOTICE ) ) ) {
			$notices[] = $notice;
		}
		return apply_filters( 'sip_wc_url_coupons_get_notices', array_unique( array_filter( $notices ) ) );
	}

	/**
	 * remove_default_notice.
	 *
	 * @version 1.4.0
	 * @since   1.4.0
	 */
	function remove_default_notice( $msg, $msg_code, $coupon ) {
		if ( '1' != get_option( SIP_WC_URL_COUPONS_NOTICE_REMOVE_DEFAULT) ) {
			return $msg;
		}
		$notices = $this->get_notices( $coupon );
		return ( ! empty( $notices ) ? '' : $msg );
	}

	/**
	 * append_notice.
	 *
	 * @version 1.4.0
	 * @since   1.4.0
	 */
	function append_notice( $msg, $msg_code, $coupon ) {
		if ( WC_Coupon::WC_COUPON_SUCCESS != $msg_code ) {
			return $msg;
		}
		if ( 'append' != get_option( SIP_WC_URL_COUPONS_NOTICE_METHOD ) ) {
			return $msg;
		}
		$notices = $this->get_notices( $coupon, $msg );
		return implode( get_option( SIP_WC_URL_COUPONS_NOTICE_GLUE ), $notices );
	}
	


}
