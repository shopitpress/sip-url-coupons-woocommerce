<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://shopitpress.com
 * @since      1.0.0
 *
 * @package    Sip_Url_Coupons_WC
 * @subpackage Sip_Url_Coupons_WC/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Sip_Url_Coupons_WC
 * @subpackage Sip_Url_Coupons_WC/includes
 * @author     shopitpress <info@shopitpress.com>
 */
class Sip_Url_Coupons_Woocommerce_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
