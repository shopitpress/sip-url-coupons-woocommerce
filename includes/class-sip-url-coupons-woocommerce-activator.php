<?php

/**
 * Fired during plugin activation
 *
 * @link       https://shopitpress.com
 * @since      1.0.0
 *
 * @package    Sip_Url_Coupons_WC
 * @subpackage Sip_Url_Coupons_WC/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Sip_Url_Coupons_WC
 * @subpackage Sip_Url_Coupons_WC/includes
 * @author     shopitpress <info@shopitpress.com>
 */
class Sip_Url_Coupons_Woocommerce_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
