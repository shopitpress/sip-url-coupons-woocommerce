<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://shopitpress.com
 * @since      1.0.0
 *
 * @package    Sip_Url_Coupons_WC
 * @subpackage Sip_Url_Coupons_WC/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Sip_Url_Coupons_WC
 * @subpackage Sip_Url_Coupons_WC/includes
 * @author     shopitpress <info@shopitpress.com>
 */
class Sip_Url_Coupons_Woocommerce {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Sip_Url_Coupons_Woocommerce_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'SIP_URL_COUPONS_WOOCOMMERCE_VERSION' ) ) {
			$this->version = SIP_URL_COUPONS_WOOCOMMERCE_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'sip-url-coupons-wc';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
		$this->define_sip_keys();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Sip_Url_Coupons_Woocommerce_Loader. Orchestrates the hooks of the plugin.
	 * - Sip_Url_Coupons_Woocommerce_i18n. Defines internationalization functionality.
	 * - Sip_Url_Coupons_Woocommerce_Admin. Defines all hooks for the admin area.
	 * - Sip_Url_Coupons_Woocommerce_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-sip-url-coupons-woocommerce-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-sip-url-coupons-woocommerce-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-sip-url-coupons-woocommerce-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-sip-url-coupons-woocommerce-public.php';

		$this->loader = new Sip_Url_Coupons_Woocommerce_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Sip_Url_Coupons_Woocommerce_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {
		$plugin_i18n = new Sip_Url_Coupons_Woocommerce_i18n();
		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );
	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Sip_Url_Coupons_Woocommerce_Admin( $this->get_plugin_name(), $this->get_version() );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'shopitpress_plugin_admin_menu' );
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Sip_Url_Coupons_Woocommerce_Public( $this->get_plugin_name(), $this->get_version() );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
		if (get_option( SIP_WC_URL_COUPONS_ENABLED, 'yes' ) ) {			
			// Apply URL coupon
			add_action( 'wp_loaded', array( $this, 'apply_url_coupon' ), ( '' !== ( $priority = get_option( SIP_WC_URL_COUPONS_PRIORITY ) ) ? $priority : PHP_INT_MAX ) );
			add_action( 'sip_wc_url_coupons_before_coupon_applied', array( $this, 'maybe_force_start_session' ), 10 );
			add_action( 'sip_wc_url_coupons_before_coupon_applied', array( $this, 'maybe_set_additional_cookie' ), 11 );
			if ( get_option( 'sip_wc_url_coupons_delay_notice' ) ) {
				add_action( 'sip_wc_url_coupons_after_coupon_applied', array( $this, 'delay_notice' ), 10, 3 );
				add_action( 'wp_head', array( $this, 'display_delayed_notice' ) );
			}
			add_action( 'sip_wc_url_coupons_after_coupon_applied', array( $this, 'redirect' ), PHP_INT_MAX, 3 );
			// Hide coupons			
			if ( get_option( SIP_WC_URL_COUPONS_CART_HIDE_COUPON ) ) {
				add_filter( 'woocommerce_coupons_enabled', array( $this, 'hide_coupon_field_on_cart' ), PHP_INT_MAX );
			}
			if ( get_option( SIP_WC_URL_COUPONS_CHECKOUT_HIDE_COUPON ) ) {
				add_filter( 'woocommerce_coupons_enabled', array( $this, 'hide_coupon_field_on_checkout' ), PHP_INT_MAX );
			}
			// Force coupon redirect
			if (  get_option( SIP_WC_URL_COUPONS_ADD_TO_CART_ACTION_FORCE_COUPON_REDIRECT ) ) {
				add_filter( 'woocommerce_add_to_cart_redirect', array( $this, 'add_to_cart_action_force_coupon_redirect' ), PHP_INT_MAX, 2 );
			}
			// WP Rocket: Disable empty cart caching
			if (  get_option( SIP_WC_URL_COUPONS_WP_ROCKET_DISABLE_CACHE_WC_EMPTY_CART ) ) {
				add_filter( 'rocket_cache_wc_empty_cart', '__return_false', PHP_INT_MAX );
			}
		}
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Sip_Url_Coupons_Woocommerce_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}
	
	/**
	 * hide_coupon_field_on_cart.
	 *
	 * @version 1.0.0
	 * @since   1.0.0
	 */
	public function hide_coupon_field_on_cart( $enabled ) {
		return ( is_cart() ? false : $enabled );
	}
	/**
	 * hide_coupon_field_on_checkout.
	 *
	 * @version 1.2.1
	 * @since   1.2.1
	 */
	public function hide_coupon_field_on_checkout( $enabled ) {
		return ( is_checkout() ? false : $enabled );
	}
	/**
	 * apply_url_coupon.
	 *
	 * @version 1.4.0
	 * @since   1.0.0
	 * @todo    [next] use `WC()->cart->apply_coupon()` instead of `WC()->cart->add_discount()`
	 * @todo    [maybe] options to add products to cart with query arg
	 * @todo    [maybe] `if ( ! WC()->cart->has_discount( $coupon_code ) ) {}`
	 */
	public function apply_url_coupon() {
		$key = get_option( SIP_WC_URL_COUPONS_KEY, 'sip_apply_coupon' );
		if ( isset( $_GET[ $key ] ) && '' !== $_GET[ $key ] && function_exists( 'WC' ) ) {
			$coupon_code = sanitize_text_field( $_GET[ $key ] );
			do_action( 'sip_wc_url_coupons_before_coupon_applied', $coupon_code, $key );
			$result = WC()->cart->add_discount( $coupon_code );
			do_action( 'sip_wc_url_coupons_after_coupon_applied',  $coupon_code, $key, $result );
		}
	}
	/**
	 * maybe_force_start_session.
	 *
	 * @version 1.3.0
	 * @since   1.3.0
	 */
	public function maybe_force_start_session( $coupon_code ) {
		if ( 'yes' === get_option( SIP_WC_URL_COUPONS_FORCE_START_SESSION, 'no' ) && WC()->session && ! WC()->session->has_session() ) {
			WC()->session->set_customer_session_cookie( true );
		}
	}
	/**
	 * maybe_set_additional_cookie.
	 *
	 * @version 1.3.0
	 * @since   1.3.0
	 */
	public function maybe_set_additional_cookie( $coupon_code ) {
		if ( 'yes' === get_option( SIP_WC_URL_COUPONS_COOKIE_ENABLED, 'no' ) ) {
			setcookie( 'sip_wc_url_coupons', $coupon_code, ( time() + get_option( SIP_WC_URL_COUPONS_COOKIE_SEC, 1209600 ) ), '/', $_SERVER['SERVER_NAME'], false );
		}
	}
	/**
	 * delay_notice.
	 *
	 * @version 1.4.0
	 * @since   1.3.0
	 * @todo    [maybe] still delay notice on `! $result`?
	 */
	public function delay_notice( $coupon_code, $key, $result ) {
		if ( ! $result ) {
			return;
		}
		if ( WC()->cart->is_empty() ) {
			$all_notices = WC()->session->get( 'wc_notices', array() );
			wc_clear_notices();
			WC()->session->set( 'sip_wc_url_coupons_notices', $all_notices );
		}
	}
	/**
	 * display_delayed_notice.
	 *
	 * @version 1.3.0
	 * @since   1.2.5
	 */
	public function display_delayed_notice() {
		if ( function_exists( 'WC' ) && ! WC()->cart->is_empty() && ( $notices = WC()->session->get( 'sip_wc_url_coupons_notices', array() ) ) && ! empty( $notices ) ) {
			WC()->session->set( 'sip_wc_url_coupons_notices', null );
			WC()->session->set( 'wc_notices', $notices );
		}
	}
	/**
	 * redirect.
	 *
	 * @version 1.4.0
	 * @since   1.3.0
	 * @todo    [maybe] different/same redirect on `! $result`?
	 */
	public function redirect( $coupon_code, $key, $result ) {
		if ( ! $result ) {
			return;
		}
		$keys_to_remove = array( $key );
		if ( 'yes' === get_option( SIP_WC_URL_COUPONS_REMOVE_ADD_TO_CART_KEY, 'yes' ) ) {
			$keys_to_remove[] = 'add-to-cart';
		}
		wp_safe_redirect( apply_filters( 'sip_wc_url_coupons_redirect_url', remove_query_arg( $keys_to_remove ), $coupon_code, $key ) );
		exit;
	}
	/**
	 * add_to_cart_action_force_coupon_redirect.
	 *
	 * @version 1.3.2
	 * @since   1.3.2
	 */
	public function add_to_cart_action_force_coupon_redirect( $url, $adding_to_cart ) {
		$key = get_option( SIP_WC_URL_COUPONS_KEY, 'sip_apply_coupon' );
		return ( isset( $_GET[ $key ] ) ? remove_query_arg( 'add-to-cart' ) : $url );
	}	
	public function define_sip_keys(){
		define( 'SIP_WC_URL_COUPONS_ENABLED', 'sip_wc_url_coupons_enabled' );
		//General
		define( 'SIP_WC_URL_COUPONS_KEY', 'sip_wc_url_coupons_key' );
		define( 'SIP_WC_URL_COUPONS_FORCE_START_SESSION', 'sip_wc_url_coupons_force_start_session' );
		//Hide coupon option
		define( 'SIP_WC_URL_COUPONS_CART_HIDE_COUPON', 'sip_wc_url_coupons_cart_hide_coupon' );
		define( 'SIP_WC_URL_COUPONS_CHECKOUT_HIDE_COUPON', 'sip_wc_url_coupons_checkout_hide_coupon' );
		// advance option
		define( 'SIP_WC_URL_COUPONS_PRIORITY', 'sip_wc_url_coupons_priority' );
		define( 'SIP_WC_URL_COUPONS_REMOVE_ADD_TO_CART_KEY', 'sip_wc_url_coupons_remove_add_to_cart_key' );
		define( 'SIP_WC_URL_COUPONS_ADD_TO_CART_ACTION_FORCE_COUPON_REDIRECT', 'sip_wc_url_coupons_add_to_cart_action_force_coupon_redirect' );
		define( 'SIP_WC_URL_COUPONS_COOKIE_ENABLED', 'sip_wc_url_coupons_cookie_enabled' );
		define( 'SIP_WC_URL_COUPONS_COOKIE_SEC', 'sip_wc_url_coupons_cookie_sec' );
		define( 'SIP_WC_URL_COUPONS_WP_ROCKET_DISABLE_CACHE_WC_EMPTY_CART', 'sip_wc_url_coupons_wp_rocket_disable_cache_wc_empty_cart' );
		define( 'SIP_WC_URL_COUPONS__RESET', 'sip_wc_url_coupons__reset' );		    			
	}
	
}
