<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://shopitpress.com
 * @since      1.0.0
 *
 * @package    Sip_Url_Coupons_WC
 * @subpackage Sip_Url_Coupons_WC/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
