<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://shopitpress.com
 * @since      1.0.0
 *
 * @package    Sip_Url_Coupons_WC
 * @subpackage Sip_Url_Coupons_WC/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<?php 
 $sip_wc_url_coupons_enabled=get_option(SIP_WC_URL_COUPONS_ENABLED);
 $sip_wc_url_coupons_key=get_option(SIP_WC_URL_COUPONS_KEY); 
 $sip_wc_url_coupons_key = !empty($sip_wc_url_coupons_key) ? $sip_wc_url_coupons_key :"sip_apply_coupon";
 $sip_wc_url_coupons_force_start_session=get_option(SIP_WC_URL_COUPONS_FORCE_START_SESSION); 
 $sip_wc_url_coupons_cart_hide_coupon=get_option(SIP_WC_URL_COUPONS_CART_HIDE_COUPON); $sip_wc_url_coupons_checkout_hide_coupon=get_option(SIP_WC_URL_COUPONS_CHECKOUT_HIDE_COUPON); 

 $sip_wc_url_coupons_priority=get_option(SIP_WC_URL_COUPONS_PRIORITY); 
 $sip_wc_url_coupons_remove_add_to_cart_key=get_option(SIP_WC_URL_COUPONS_REMOVE_ADD_TO_CART_KEY); 
 $sip_wc_url_coupons_add_to_cart_action_force_coupon_redirect=get_option(SIP_WC_URL_COUPONS_ADD_TO_CART_ACTION_FORCE_COUPON_REDIRECT); 
 $sip_wc_url_coupons_cookie_enabled=get_option(SIP_WC_URL_COUPONS_COOKIE_ENABLED); 
 $sip_wc_url_coupons_cookie_sec=get_option(SIP_WC_URL_COUPONS_COOKIE_SEC); 
 $sip_wc_url_coupons_cookie_sec = !empty($sip_wc_url_coupons_cookie_sec) ? $sip_wc_url_coupons_cookie_sec :"1209600";
 $sip_wc_url_coupons_wp_rocket_disable_cache_wc_empty_cart=get_option(SIP_WC_URL_COUPONS_WP_ROCKET_DISABLE_CACHE_WC_EMPTY_CART); 
?>
<div class="wrap">
    <form method="post" id="mainform" action="" enctype="multipart/form-data">
        <h1 class="">SIP Url Coupons</h1>
        <h2>URL Coupons Options</h2>
        <table class="form-table">
            <tbody>
                <tr valign="top" class="">
                    <th scope="row" class="titledesc">URL Coupons</th>
                    <td class="forminp forminp-checkbox">
                        <fieldset>
                            <legend class="screen-reader-text"><span>URL Coupons</span></legend>
                            <label for="<?=  SIP_WC_URL_COUPONS_ENABLED ?>">
                                <input name="<?=  SIP_WC_URL_COUPONS_ENABLED ?>" id="<?=  SIP_WC_URL_COUPONS_ENABLED ?>"
                                    type="checkbox"  value="1"  <?php echo ($sip_wc_url_coupons_enabled ? 'checked="checked"' :'' ) ?> > <strong>Enable plugin</strong>
                            </label>
                        </fieldset>
                    </td>
                </tr>
            </tbody>
        </table>
        <h2>General Options</h2>
        <table class="form-table">
            <tbody>
                <tr valign="top">
                    <th scope="row" class="titledesc">
                        <label for="<?=  SIP_WC_URL_COUPONS_KEY ?>">URL coupons key</label>
                    </th>
                    <td class="forminp forminp-text">
                        <input name="<?=  SIP_WC_URL_COUPONS_KEY ?>" id="<?=  SIP_WC_URL_COUPONS_KEY ?>" type="text" 
                            value="<?= $sip_wc_url_coupons_key;?>"  placeholder="">
                        <p class="description"></p>
                        <p>Your customers can apply shop's standard coupons by visiting URL. E.g.:
                            <code><?= site_url();?>/?<strong><?= $sip_wc_url_coupons_key;?></strong>=couponcode</code>.
                        </p>                        
                    </td>
                </tr>
                <tr valign="top" class="">
                    <th scope="row" class="titledesc">Force session start</th>
                    <td class="forminp forminp-checkbox">
                        <fieldset>
                            <legend class="screen-reader-text"><span>Force session start</span></legend>
                            <label for="<?=  SIP_WC_URL_COUPONS_FORCE_START_SESSION  ?>">
                                <input name="<?=  SIP_WC_URL_COUPONS_FORCE_START_SESSION  ?>"
                                    id="<?=  SIP_WC_URL_COUPONS_FORCE_START_SESSION  ?>" type="checkbox"  <?php echo ($sip_wc_url_coupons_force_start_session ? 'checked="checked"' :'' ) ?> value="1">
                                Enable </label>
                            <p class="description">Enable this if URL coupons are not being applied to the guests (i.e.
                                not logged users).</p>
                        </fieldset>
                    </td>
                </tr>
                <?php do_action('add_products_to_cart_display_option');?>
            </tbody>
        </table>
        <h2>Hide Coupon Options</h2>
        <table class="form-table">

            <tbody>
                <tr valign="top" class="">
                    <th scope="row" class="titledesc">Hide coupon on cart page</th>
                    <td class="forminp forminp-checkbox">
                        <fieldset>
                            <legend class="screen-reader-text"><span>Hide coupon on cart page</span></legend>
                            <label for="<?=  SIP_WC_URL_COUPONS_CART_HIDE_COUPON   ?>">
                                <input name="<?=  SIP_WC_URL_COUPONS_CART_HIDE_COUPON   ?>"
                                    id="<?=  SIP_WC_URL_COUPONS_CART_HIDE_COUPON   ?>" type="checkbox" <?php echo ($sip_wc_url_coupons_cart_hide_coupon ? 'checked="checked"' :'' ) ?>  value="1"> Hide
                            </label>
                            <p class="description">Enable this if you want to hide standard coupon input field on the cart page.</p>
                        </fieldset>
                    </td>
                </tr>
                <tr valign="top" class="">
                    <th scope="row" class="titledesc">Hide coupon on checkout page</th>
                    <td class="forminp forminp-checkbox">
                        <fieldset>
                            <legend class="screen-reader-text"><span>Hide coupon on checkout page</span></legend>
                            <label for="<?=  SIP_WC_URL_COUPONS_CHECKOUT_HIDE_COUPON    ?>">
                                <input name="<?=  SIP_WC_URL_COUPONS_CHECKOUT_HIDE_COUPON    ?>"
                                    id="<?=  SIP_WC_URL_COUPONS_CHECKOUT_HIDE_COUPON    ?>" <?php echo ($sip_wc_url_coupons_checkout_hide_coupon ? 'checked="checked"' :'' ) ?> type="checkbox" class="" value="1">
                                Hide </label>
                            <p class="description">Enable this if you want to hide standard coupon input field on the
                                checkout page.</p>
                        </fieldset>
                    </td>
                </tr>
            </tbody>
        </table>
        <h2>Advanced</h2>        
        <?php do_action('redirect_display_options');?>
        <table class="form-table">

            <tbody>
                <tr valign="top">
                    <th scope="row" class="titledesc">
                        <label for="<?=  SIP_WC_URL_COUPONS_PRIORITY ?>">Hook priority </label>
                    </th>
                    <td class="forminp forminp-number">
                        <input name="<?=  SIP_WC_URL_COUPONS_PRIORITY ?>" id="<?=  SIP_WC_URL_COUPONS_PRIORITY ?>" type="number"
                             value="<?= $sip_wc_url_coupons_priority;?>"  placeholder="">
                    </td>
                </tr>
                <tr valign="top" class="">
                    <th scope="row" class="titledesc">Remove "add to cart" key</th>
                    <td class="forminp forminp-checkbox">
                        <fieldset>
                            <legend class="screen-reader-text"><span>Remove "add to cart" key</span></legend>
                            <label for="<?=  SIP_WC_URL_COUPONS_REMOVE_ADD_TO_CART_KEY ?>">
                                <input name="<?=  SIP_WC_URL_COUPONS_REMOVE_ADD_TO_CART_KEY ?>"
                                    id="<?=  SIP_WC_URL_COUPONS_REMOVE_ADD_TO_CART_KEY ?>" type="checkbox"  value="1"
                                    <?php echo ($sip_wc_url_coupons_remove_add_to_cart_key ? 'checked="checked"' :'' ) ?> > Enable </label>
                            <p class="description">Will remove <code>add-to-cart</code> key on "Redirect URL &gt; No
                                redirect" option.</p>
                        </fieldset>
                    </td>
                </tr>
                <tr valign="top" class="">
                    <th scope="row" class="titledesc">Force coupon redirect</th>
                    <td class="forminp forminp-checkbox">
                        <fieldset>
                            <legend class="screen-reader-text"><span>Force coupon redirect</span></legend>
                            <label for="<?=  SIP_WC_URL_COUPONS_ADD_TO_CART_ACTION_FORCE_COUPON_REDIRECT  ?>">
                                <input name="<?=  SIP_WC_URL_COUPONS_ADD_TO_CART_ACTION_FORCE_COUPON_REDIRECT  ?>"
                                    id="<?=  SIP_WC_URL_COUPONS_ADD_TO_CART_ACTION_FORCE_COUPON_REDIRECT  ?>" type="checkbox"
                                    <?php echo ($sip_wc_url_coupons_add_to_cart_action_force_coupon_redirect ? 'checked="checked"' :'' ) ?> value="1"> Enable </label>
                            <p class="description">Force coupon redirect after <code>add-to-cart</code> action.</p>
                        </fieldset>
                    </td>
                </tr>
                <tr valign="top" class="">
                    <th scope="row" class="titledesc">Extra cookie</th>
                    <td class="forminp forminp-checkbox">
                        <fieldset>
                            <legend class="screen-reader-text"><span>Extra cookie</span></legend>
                            <label for="<?=  SIP_WC_URL_COUPONS_COOKIE_ENABLED  ?>">
                                <input name="<?=  SIP_WC_URL_COUPONS_COOKIE_ENABLED  ?>" id="<?=  SIP_WC_URL_COUPONS_COOKIE_ENABLED  ?>"
                                    type="checkbox"  <?php echo ($sip_wc_url_coupons_cookie_enabled ? 'checked="checked"' :'' ) ?> value="1"> Enable </label>
                            <p class="description">Enable this if you want to set cookie when URL coupon has been
                                applied. Cookie name will be <code>sip_wc_url_coupons</code>.</p>
                        </fieldset>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row" class="titledesc">
                        <label for="<?=  SIP_WC_URL_COUPONS_COOKIE_SEC  ?>"> </label>
                    </th>
                    <td class="forminp forminp-number">
                        <input name="<?=  SIP_WC_URL_COUPONS_COOKIE_SEC  ?>" id="<?=  SIP_WC_URL_COUPONS_COOKIE_SEC  ?>" type="number"
                            style="" value="<?= $sip_wc_url_coupons_cookie_sec; ?>"  min="1">
                        <p class="description">The time the cookie expires. In seconds.</p>
                    </td>
                </tr>
                <tr valign="top" class="">
                    <th scope="row" class="titledesc">WP Rocket: Disable empty cart caching</th>
                    <td class="forminp forminp-checkbox">
                        <fieldset>
                            <legend class="screen-reader-text"><span>WP Rocket: Disable empty cart caching</span>
                            </legend>
                            <label for="<?=  SIP_WC_URL_COUPONS_WP_ROCKET_DISABLE_CACHE_WC_EMPTY_CART   ?>">
                                <input name="<?=  SIP_WC_URL_COUPONS_WP_ROCKET_DISABLE_CACHE_WC_EMPTY_CART   ?>"
                                    id="<?=  SIP_WC_URL_COUPONS_WP_ROCKET_DISABLE_CACHE_WC_EMPTY_CART   ?>" type="checkbox"
                                    <?php echo ($sip_wc_url_coupons_wp_rocket_disable_cache_wc_empty_cart ? 'checked="checked"' :'' ) ?> value="1"> Disable </label>
                            <p class="description">Check this if you have "WP Rocket" plugin installed, and having
                                issues with cart being empty after you apply URL coupon and add a product.</p>
                        </fieldset>
                    </td>
                </tr>
            </tbody>
        </table>
        <h2>Notes</h2>
        <div id="sip_wc_url_coupons_notes-description">
            <p><span class="dashicons dashicons-info"></span> If you are using URL to a page where no WooCommerce
                notices are displayed, try adding our <code>[sip_wc_url_coupons_print_notices]</code> shortcode to the
                content. Please note that this shortcode will print all WooCommerce notices (i.e. not only from our
                plugin, or notices related to the coupons).</p>
        </div>
        <table class="form-table">

        </table>
        <h2>Reset Settings</h2>
        <table class="form-table">

            <tbody>
                <tr valign="top" class="">
                    <th scope="row" class="titledesc">Reset section settings</th>
                    <td class="forminp forminp-checkbox">
                        <fieldset>
                            <legend class="screen-reader-text"><span>Reset section settings</span></legend>
                            <label for="<?= SIP_WC_URL_COUPONS__RESET ?>">
                                <input name="<?= SIP_WC_URL_COUPONS__RESET ?>" id="<?= SIP_WC_URL_COUPONS__RESET ?>" type="checkbox"
                                    class="" value="1"> <strong>Reset</strong> </label>
                            <p class="description">Check the box and save changes to reset.</p>
                        </fieldset>
                    </td>
                </tr>
            </tbody>
        </table>
        <p class="submit">
            <button name="save" class="button-primary " type="submit" value="Save changes">Save
                changes</button>
				<?php wp_nonce_field('sip_save_options'); ?>
        </p>
    </form>
</div>