<?php
add_action( 'add_products_to_cart_display_option', 'add_products_to_cart_display_option', 10);
add_action( 'redirect_display_options', 'redirect_display_options', 10);
add_action( 'sip_url_coupon_woocommerce_save_option_pro', 'sip_url_coupon_woocommerce_save_option_pro', 10);
    /**
	 * display in admin option for pro version.
	 *
	 * @since    1.0.0
	 */
	function add_products_to_cart_display_option() {  
        $sip_wc_url_coupons_fixed_product_discount_add_products=get_option(SIP_WC_URL_COUPONS_FIXED_PRODUCT_DISCOUNT_ADD_PRODUCTS);
        $sip_wc_url_coupons_fixed_product_discount_add_products_emptycart=get_option(SIP_WC_URL_COUPONS_FIXED_PRODUCT_DISCOUNT_ADD_PRODUCTS_EMPTYCART);

        $sip_wc_url_coupons_redirect=get_option(SIP_WC_URL_COUPONS_REDIRECT);        
        $sip_wc_url_coupons_redirect_custom_url=get_option(SIP_WC_URL_COUPONS_REDIRECT_CUSTOM_URL);
        $sip_wc_url_coupons_redirect_per_coupon=get_option(SIP_WC_URL_COUPONS_REDIRECT_PER_COUPON);        
        ?>
<tr valign="top" >
    <th scope="row" class="titledesc">Add products to cart</th>
    <td class="forminp forminp-checkbox">
        <fieldset>
            <legend class="screen-reader-text"><span>Add products to cart</span></legend>
            <label for="<?=  SIP_WC_URL_COUPONS_FIXED_PRODUCT_DISCOUNT_ADD_PRODUCTS ?>">
                <input name="<?=  SIP_WC_URL_COUPONS_FIXED_PRODUCT_DISCOUNT_ADD_PRODUCTS  ?>"
                    id="<?=  SIP_WC_URL_COUPONS_FIXED_PRODUCT_DISCOUNT_ADD_PRODUCTS ?>" type="checkbox" value="1"
                    <?php echo ($sip_wc_url_coupons_fixed_product_discount_add_products ? 'checked="checked"' :'' ) ?>>
                Enable</label>
            <p class="description">Automatically adds coupon's products to the cart for "Fixed product discount" type
                coupons.</p>
        </fieldset>
        <fieldset>
            <label for="<?=  SIP_WC_URL_COUPONS_FIXED_PRODUCT_DISCOUNT_ADD_PRODUCTS_EMPTYCART  ?>">
                <input name="<?=  SIP_WC_URL_COUPONS_FIXED_PRODUCT_DISCOUNT_ADD_PRODUCTS_EMPTYCART  ?>"
                    id="<?=  SIP_WC_URL_COUPONS_FIXED_PRODUCT_DISCOUNT_ADD_PRODUCTS_EMPTYCART ?>" type="checkbox"
                    value="1"
                    <?php echo ($sip_wc_url_coupons_fixed_product_discount_add_products_emptycart ? 'checked="checked"' :'' ) ?>>
                Empty cart</label>
            <p class="description">Clear the cart before adding the coupon's products.</p>
        </fieldset>
    </td>
</tr>
<?php }
        /**
	 * display in admin option for pro version.
	 *
	 * @since    1.0.0
	 */
	function redirect_display_options() {  
        $sip_wc_url_coupons_redirect=get_option(SIP_WC_URL_COUPONS_REDIRECT);        
        $sip_wc_url_coupons_redirect_custom_url=get_option(SIP_WC_URL_COUPONS_REDIRECT_CUSTOM_URL);
        $sip_wc_url_coupons_redirect_per_coupon=get_option(SIP_WC_URL_COUPONS_REDIRECT_PER_COUPON);  
        
        $sip_wc_url_coupons_delay_notice=get_option(SIP_WC_URL_COUPONS_DELAY_NOTICE);        
        $sip_wc_url_coupons_notice=get_option(SIP_WC_URL_COUPONS_NOTICE);
        $sip_wc_url_coupons_notice_per_coupon=get_option(SIP_WC_URL_COUPONS_NOTICE_PER_COUPON);  
        $sip_wc_url_coupons_notice_per_coupon_override=get_option(SIP_WC_URL_COUPONS_NOTICE_PER_COUPON_OVERRIDE);        
        $sip_wc_url_coupons_notice_remove_default=get_option(SIP_WC_URL_COUPONS_NOTICE_REMOVE_DEFAULT);
        $sip_wc_url_coupons_notice_method=get_option(SIP_WC_URL_COUPONS_NOTICE_METHOD);  
        $sip_wc_url_coupons_notice_type=get_option(SIP_WC_URL_COUPONS_NOTICE_TYPE);        
        $sip_wc_url_coupons_notice_glue=get_option(SIP_WC_URL_COUPONS_NOTICE_GLUE);	
        ?>
<h2>Redirect Options</h2>
<table class="form-table">
    <tbody>
        <tr valign="top">
            <th scope="row" class="titledesc">
                <label for="<?= SIP_WC_URL_COUPONS_REDIRECT ?>">Redirect URL</label>
            </th>
            <td class="forminp forminp-select">
                <select name="<?= SIP_WC_URL_COUPONS_REDIRECT ?>" id="<?= SIP_WC_URL_COUPONS_REDIRECT ?>" class="chosen_select  ">
                    <option value="no"  <?php echo ((isset($sip_wc_url_coupons_redirect) && $sip_wc_url_coupons_redirect=='no') || empty($sip_wc_url_coupons_redirect)? 'selected="selected"' :'' ) ?> >No redirect</option>
                    <option value="cart" <?php echo (isset($sip_wc_url_coupons_redirect) && $sip_wc_url_coupons_redirect =='cart' ? 'selected="selected"' :'' ) ?>>Redirect to cart</option>
                    <option value="checkout" <?php echo (isset($sip_wc_url_coupons_redirect) && $sip_wc_url_coupons_redirect=='checkout' ? 'selected="selected"' :'' ) ?>>Redirect to checkout</option>
                    <option value="custom" <?php echo (isset($sip_wc_url_coupons_redirect) && $sip_wc_url_coupons_redirect=='custom' ? 'selected="selected"' :'' ) ?>>Redirect to custom local URL</option>
                </select>
            </td>
        </tr>
        <tr valign="top">
            <th scope="row" class="titledesc">
                <label for="<?= SIP_WC_URL_COUPONS_REDIRECT_CUSTOM_URL ?>"> </label>
            </th>
            <td class="forminp forminp-text">
                <input name="<?= SIP_WC_URL_COUPONS_REDIRECT_CUSTOM_URL ?>" id="<?= SIP_WC_URL_COUPONS_REDIRECT_CUSTOM_URL ?>" type="text" style="width:50%;" value="<?php echo isset($sip_wc_url_coupons_redirect_custom_url) && !empty($sip_wc_url_coupons_redirect_custom_url) ? $sip_wc_url_coupons_redirect_custom_url : "";?>">
                <p class="description">Custom local URL</p>
            </td>
        </tr>
        <tr valign="top" >
            <th scope="row" class="titledesc">Redirect URL per coupon</th>
            <td class="forminp forminp-checkbox">
                <fieldset>
                    <legend class="screen-reader-text"><span>Redirect URL per coupon</span></legend>
                    <label for="<?= SIP_WC_URL_COUPONS_REDIRECT_PER_COUPON ?>">
                        <input name="<?= SIP_WC_URL_COUPONS_REDIRECT_PER_COUPON ?>" id="<?= SIP_WC_URL_COUPONS_REDIRECT_PER_COUPON ?>"
                            type="checkbox" <?php echo (isset($sip_wc_url_coupons_redirect_per_coupon) && $sip_wc_url_coupons_redirect_per_coupon=='1' ? 'checked="checked"' :'' ) ?>  value="1"> Enable
                    </label>
                    <p class="description">Will add "URL Coupons: Redirect URL" field to each coupon admin edit page.
                    </p>
                </fieldset>
            </td>
        </tr>
    </tbody>
</table>
<h2>Notice Options</h2>
<table class="form-table">
    <tbody>
        <tr valign="top">
            <th scope="row" class="titledesc">Delay notice</th>
            <td class="forminp forminp-checkbox">
                <fieldset>
                    <legend class="screen-reader-text"><span>Delay notice</span></legend>
                    <label for="<?= SIP_WC_URL_COUPONS_DELAY_NOTICE ?>">
                        <input name="<?= SIP_WC_URL_COUPONS_DELAY_NOTICE ?>" id="<?= SIP_WC_URL_COUPONS_DELAY_NOTICE ?>"
                            type="checkbox"  <?php echo (isset($sip_wc_url_coupons_delay_notice) && $sip_wc_url_coupons_delay_notice=='1' ? 'checked="checked"' :'' ) ?> value="1"> Delay</label>
                    <p class="description">Delay the "Coupon code applied successfully" notice if the cart is empty when
                        applying a URL coupon. Notice will be delayed until there is at least one product in the cart.
                    </p>
                </fieldset>
            </td>
        </tr>
        <tr valign="top">
            <th scope="row" class="titledesc">
                <label for="<?= SIP_WC_URL_COUPONS_NOTICE ?>">Custom notice </label>
            </th>
            <td class="forminp forminp-textarea">

                <textarea name="<?= SIP_WC_URL_COUPONS_NOTICE ?>" id="<?= SIP_WC_URL_COUPONS_NOTICE ?>" style="width:100%;"><?php echo (isset($sip_wc_url_coupons_notice) && !empty($sip_wc_url_coupons_notice) ? $sip_wc_url_coupons_notice :'' ) ?></textarea>
            </td>
        </tr>
        <tr valign="top" >
            <th scope="row" class="titledesc">Notice per coupon</th>
            <td class="forminp forminp-checkbox">
                <fieldset>
                    <legend class="screen-reader-text"><span>Notice per coupon</span></legend>
                    <label for="<?= SIP_WC_URL_COUPONS_NOTICE_PER_COUPON  ?>">
                        <input name="<?= SIP_WC_URL_COUPONS_NOTICE_PER_COUPON  ?>" id="<?= SIP_WC_URL_COUPONS_NOTICE_PER_COUPON  ?>"
                        <?php echo (isset($sip_wc_url_coupons_notice_per_coupon) && $sip_wc_url_coupons_notice_per_coupon=='1' ? 'checked="checked"' :'' ) ?>   type="checkbox"  value="1"> Enable </label>
                    <p class="description">Will add "URL Coupons: Notice" field to each coupon admin edit page.</p>
                </fieldset>
                <fieldset >
                    <label for="<?= SIP_WC_URL_COUPONS_NOTICE_PER_COUPON_OVERRIDE  ?>">
                        <input name="<?= SIP_WC_URL_COUPONS_NOTICE_PER_COUPON_OVERRIDE  ?>"
                        <?php echo (isset($sip_wc_url_coupons_notice_per_coupon_override) && $sip_wc_url_coupons_notice_per_coupon_override=='1' ? 'checked="checked"' :'' ) ?>  id="<?= SIP_WC_URL_COUPONS_NOTICE_PER_COUPON_OVERRIDE  ?>" type="checkbox"  value="1">
                        Override global notice </label>
                    <p class="description">This will remove the global "Custom notice" notice, in case if there are any
                        "per coupon" notices to display.</p>
                </fieldset>
            </td>
        </tr>
        <tr valign="top" >
            <th scope="row" class="titledesc">Override default notice</th>
            <td class="forminp forminp-checkbox">
                <fieldset>
                    <legend class="screen-reader-text"><span>Override default notice</span></legend>
                    <label for="<?= SIP_WC_URL_COUPONS_NOTICE_REMOVE_DEFAULT?>">
                        <input name="<?= SIP_WC_URL_COUPONS_NOTICE_REMOVE_DEFAULT?>"
                        <?php echo (isset($sip_wc_url_coupons_notice_remove_default) && $sip_wc_url_coupons_notice_remove_default=='1' ? 'checked="checked"' :'' ) ?> id="<?= SIP_WC_URL_COUPONS_NOTICE_REMOVE_DEFAULT?>" type="checkbox"  value="1"> Enable
                    </label>
                    <p class="description">This will remove the default "Coupon code applied successfully" notice, in
                        case if there are any custom notices to display.</p>
                </fieldset>
            </td>
        </tr>
        <tr valign="top">
            <th scope="row" class="titledesc">
                <label for="<?= SIP_WC_URL_COUPONS_NOTICE_METHOD?>">Notice method <span
                        class="woocommerce-help-tip"></span></label>
            </th>
            <td class="forminp forminp-select">
                <select name="<?= SIP_WC_URL_COUPONS_NOTICE_METHOD?>" id="<?= SIP_WC_URL_COUPONS_NOTICE_METHOD?>" 
                    class="chosen_select "  >
                    <option value="add" <?php echo ((isset($sip_wc_url_coupons_notice_method) && $sip_wc_url_coupons_notice_method=='add') || empty($sip_wc_url_coupons_notice_method)? 'selected="selected"' :'' ) ?> >Add</option>
                    <option value="append"  <?php echo (isset($sip_wc_url_coupons_notice_method) && $sip_wc_url_coupons_notice_method=='append' ? 'selected="selected"' :'' ) ?> >Append</option>
                </select>
            </td>
        </tr>
        <tr valign="top">
            <th scope="row" class="titledesc">
                <label for="<?= SIP_WC_URL_COUPONS_NOTICE_TYPE?>"> </label>
            </th>
            <td class="forminp forminp-select">
                <select name="<?= SIP_WC_URL_COUPONS_NOTICE_TYPE?>" id="<?= SIP_WC_URL_COUPONS_NOTICE_TYPE?>" 
                    class="chosen_select "  >
                    <option value="success"   <?php echo ((isset($sip_wc_url_coupons_notice_type) && $sip_wc_url_coupons_notice_type=='success') || empty($sip_wc_url_coupons_notice_type)? 'selected="selected"' :'' ) ?>>Success</option>
                    <option value="error" <?php echo (isset($sip_wc_url_coupons_notice_type) && $sip_wc_url_coupons_notice_type=='error' ? 'selected="selected"' :'' ) ?> >Error</option>
                    <option value="notice" <?php echo (isset($sip_wc_url_coupons_notice_type) && $sip_wc_url_coupons_notice_type=='notice' ? 'selected="selected"' :'' ) ?>>Notice</option>
                </select>
                <p class="description">Notice type</p>
            </td>
        </tr>
        <tr valign="top">
            <th scope="row" class="titledesc">
                <label for="<?= SIP_WC_URL_COUPONS_NOTICE_GLUE?>"> </label>
            </th>
            <td class="forminp forminp-textarea">
                <p style="margin-top:0">Notice glue</p>
                <textarea name="<?= SIP_WC_URL_COUPONS_NOTICE_GLUE?>" id="<?= SIP_WC_URL_COUPONS_NOTICE_GLUE?>"  
                    > <?php echo (isset($sip_wc_url_coupons_notice_glue) && !empty($sip_wc_url_coupons_notice_glue) ? $sip_wc_url_coupons_notice_glue :'&lt;br&gt;' ) ?></textarea>
            </td>
        </tr>
    </tbody>
</table>
<?php }
    /**
	 * display in admin option for pro version.
	 *
	 * @since    1.0.0
	 */
	function sip_url_coupon_woocommerce_save_option_pro() {  

        if(isset($_POST[SIP_WC_URL_COUPONS_FIXED_PRODUCT_DISCOUNT_ADD_PRODUCTS])){
		    update_option(SIP_WC_URL_COUPONS_FIXED_PRODUCT_DISCOUNT_ADD_PRODUCTS ,$_POST[SIP_WC_URL_COUPONS_FIXED_PRODUCT_DISCOUNT_ADD_PRODUCTS]);
        }else{
            delete_option(SIP_WC_URL_COUPONS_FIXED_PRODUCT_DISCOUNT_ADD_PRODUCTS);
        }
        if(isset($_POST[SIP_WC_URL_COUPONS_FIXED_PRODUCT_DISCOUNT_ADD_PRODUCTS_EMPTYCART])){
		    update_option(SIP_WC_URL_COUPONS_FIXED_PRODUCT_DISCOUNT_ADD_PRODUCTS_EMPTYCART ,$_POST[SIP_WC_URL_COUPONS_FIXED_PRODUCT_DISCOUNT_ADD_PRODUCTS_EMPTYCART]);
        }else{
            delete_option(SIP_WC_URL_COUPONS_FIXED_PRODUCT_DISCOUNT_ADD_PRODUCTS_EMPTYCART);
        }
        
        if(isset($_POST[SIP_WC_URL_COUPONS_REDIRECT])){
		    update_option(SIP_WC_URL_COUPONS_REDIRECT ,$_POST[SIP_WC_URL_COUPONS_REDIRECT]);
        }else{
            delete_option(SIP_WC_URL_COUPONS_REDIRECT);
        }
        if(isset($_POST[SIP_WC_URL_COUPONS_REDIRECT_CUSTOM_URL])){
		    update_option(SIP_WC_URL_COUPONS_REDIRECT_CUSTOM_URL ,$_POST[SIP_WC_URL_COUPONS_REDIRECT_CUSTOM_URL]);
        }else{
            delete_option(SIP_WC_URL_COUPONS_REDIRECT_CUSTOM_URL);
        }
        if(isset($_POST[SIP_WC_URL_COUPONS_REDIRECT_PER_COUPON])){
		    update_option(SIP_WC_URL_COUPONS_REDIRECT_PER_COUPON ,$_POST[SIP_WC_URL_COUPONS_REDIRECT_PER_COUPON]);
        }else{
            delete_option(SIP_WC_URL_COUPONS_REDIRECT_PER_COUPON);
        }

        if(isset($_POST[SIP_WC_URL_COUPONS_DELAY_NOTICE])){
		    update_option(SIP_WC_URL_COUPONS_DELAY_NOTICE ,$_POST[SIP_WC_URL_COUPONS_DELAY_NOTICE]);
        }else{
            delete_option(SIP_WC_URL_COUPONS_DELAY_NOTICE);
        }
        if(isset($_POST[SIP_WC_URL_COUPONS_NOTICE])){
		    update_option(SIP_WC_URL_COUPONS_NOTICE ,$_POST[SIP_WC_URL_COUPONS_NOTICE]);
        }else{
            delete_option(SIP_WC_URL_COUPONS_NOTICE);
        }
        if(isset($_POST[SIP_WC_URL_COUPONS_NOTICE_PER_COUPON])){
		    update_option(SIP_WC_URL_COUPONS_NOTICE_PER_COUPON ,$_POST[SIP_WC_URL_COUPONS_NOTICE_PER_COUPON]);
        }else{
            delete_option(SIP_WC_URL_COUPONS_NOTICE_PER_COUPON);
        }
        if(isset($_POST[SIP_WC_URL_COUPONS_NOTICE_PER_COUPON_OVERRIDE])){
		    update_option(SIP_WC_URL_COUPONS_NOTICE_PER_COUPON_OVERRIDE ,$_POST[SIP_WC_URL_COUPONS_NOTICE_PER_COUPON_OVERRIDE]);
        }else{
            delete_option(SIP_WC_URL_COUPONS_NOTICE_PER_COUPON_OVERRIDE);
        }
        if(isset($_POST[SIP_WC_URL_COUPONS_NOTICE_REMOVE_DEFAULT])){
		    update_option(SIP_WC_URL_COUPONS_NOTICE_REMOVE_DEFAULT ,$_POST[SIP_WC_URL_COUPONS_NOTICE_REMOVE_DEFAULT]);
        }else{
            delete_option(SIP_WC_URL_COUPONS_NOTICE_REMOVE_DEFAULT);
        }
        if(isset($_POST[SIP_WC_URL_COUPONS_NOTICE_METHOD])){
		    update_option(SIP_WC_URL_COUPONS_NOTICE_METHOD ,$_POST[SIP_WC_URL_COUPONS_NOTICE_METHOD]);
        }else{
            delete_option(SIP_WC_URL_COUPONS_NOTICE_METHOD);
        }
        if(isset($_POST[SIP_WC_URL_COUPONS_NOTICE_TYPE])){
		    update_option(SIP_WC_URL_COUPONS_NOTICE_TYPE ,$_POST[SIP_WC_URL_COUPONS_NOTICE_TYPE]);
        }else{
            delete_option(SIP_WC_URL_COUPONS_NOTICE_TYPE);
        }
        if(isset($_POST[SIP_WC_URL_COUPONS_NOTICE_GLUE])){
		    update_option(SIP_WC_URL_COUPONS_NOTICE_GLUE ,$_POST[SIP_WC_URL_COUPONS_NOTICE_GLUE]);
        }else{
            delete_option(SIP_WC_URL_COUPONS_NOTICE_GLUE);
        }

        if(isset($_POST[SIP_WC_URL_COUPONS__RESET])){
            delete_option(SIP_WC_URL_COUPONS_FIXED_PRODUCT_DISCOUNT_ADD_PRODUCTS);           
            delete_option(SIP_WC_URL_COUPONS_FIXED_PRODUCT_DISCOUNT_ADD_PRODUCTS_EMPTYCART);      
            
            delete_option(SIP_WC_URL_COUPONS_REDIRECT);
            delete_option(SIP_WC_URL_COUPONS_REDIRECT_CUSTOM_URL); 
            delete_option(SIP_WC_URL_COUPONS_REDIRECT_PER_COUPON); 

            delete_option(SIP_WC_URL_COUPONS_DELAY_NOTICE);
            delete_option(SIP_WC_URL_COUPONS_NOTICE); 
            delete_option(SIP_WC_URL_COUPONS_NOTICE_PER_COUPON); 
            delete_option(SIP_WC_URL_COUPONS_NOTICE_PER_COUPON_OVERRIDE);
            delete_option(SIP_WC_URL_COUPONS_NOTICE_REMOVE_DEFAULT); 
            delete_option(SIP_WC_URL_COUPONS_NOTICE_METHOD); 
            delete_option(SIP_WC_URL_COUPONS_NOTICE_TYPE);
            delete_option(SIP_WC_URL_COUPONS_NOTICE_GLUE);             
        }
    }
?>